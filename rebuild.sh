#!/bin/bash

#Clean
rm -rf pkg
mkdir pkg

#Rebuild

for folder in ./src/*
do
	echo "$folder"
	cd "$folder"
	makepkg -sf
	cd -
done


#Move
find -iname "*.pkg.tar.*" | xargs -I{} mv {} pkg/

#Update index
repo-add pkg/jdr.db.tar.gz pkg/*.pkg.tar.* 
